using UnityEngine;

[CreateAssetMenu]
public class ScriptableObjectExample : ScriptableObject
{
    [SerializeField]
    string stringTest;
    [SerializeField]
    string version;

    public string StringTest => stringTest;
    public string Version => version;
}
