using System.Collections.Generic;
using RotaryHeart.Lib;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class Example : MonoBehaviour
{
    [SerializeField]
    Sprite defaultImage;
    [SerializeField]
    Image image;
    [SerializeField]
    Image background;
    [SerializeField]
    GameObject updateAvailableHolder;
    [SerializeField]
    TMP_Text console;
    [SerializeField]
    TMP_Text dlc1Data;
    [SerializeField]
    TMP_Text dlc2Data;
    [SerializeField]
    TMP_Text dlc3Data;
    
    //This shouldn't be used, since your server shouldn't be holding old catalog data separate
    const string OriginalCatalogPath = "https://rotaryheart.com/RemoteCatalogTest/Old/StandaloneWindows/catalog_2022.08.28.15.39.03.json";
    const string NewCatalogPath = "https://rotaryheart.com/RemoteCatalogTest/Update/StandaloneWindows/catalog_2022.08.28.15.39.03.json";

    string[] dlcCatalogs = new string[]
    {
        "https://rotaryheart.com/RemoteCatalogTest/Multiple/StandaloneWindows/DLC01_2022.10.01.16.54.34.json",
        "https://rotaryheart.com/RemoteCatalogTest/Multiple/StandaloneWindows/DLC02_2022.10.01.16.54.34.json",
        "https://rotaryheart.com/RemoteCatalogTest/Multiple/StandaloneWindows/DLC03_2022.10.01.16.54.34.json"
    };

    AsyncOperationHandle<Sprite> m_imageHandle;
    AsyncOperationHandle<Sprite> m_backgroundHandle;
    Dictionary<string, AsyncOperationHandle<ScriptableObjectExample>> m_dlcCatalogHandles;
    IResourceLocator m_updateLocator;
    KeyValuePair<string, string>? m_dlcToDownload;

    async void Start()
    {
        //Load your installed catalogs
        string savedHash = PlayerPrefs.GetString("InstalledCatalog", string.Empty);
        
        m_dlcCatalogHandles = new Dictionary<string, AsyncOperationHandle<ScriptableObjectExample>>(3);
        
        if (!string.IsNullOrEmpty(savedHash) && await AddressablesCatalogHandler.LoadLocalCatalog(savedHash))
        {
            //Load any asset from the catalogs
            //(This might not be required in your game since in most cases the catalogs are loaded in a splash/loading screen)
            LoadData();
        }

        //Load your installed dlc catalogs
        for (int i = 1; i < 4; i++)
        {
            savedHash = PlayerPrefs.GetString("DLC" + i, string.Empty);

            if (!string.IsNullOrEmpty(savedHash) && await AddressablesCatalogHandler.LoadLocalCatalog(savedHash))
            {
                //Load any asset from the catalogs
                LoadDLCData("DLC" + i);
            }
        }
    }

    void Update()
    {
        console.text = "Console:\n--" + string.Join("\n--", AddressablesCatalogHandler.consoleOutput);
    }
    
    /// <summary>
    /// Not usable code, this is just for demonstration of updating
    /// </summary>
    public async void ForceOld()
    {
        image.sprite = defaultImage;
        background.sprite = null;
        
        DeleteCache();

        if ((await AddressablesCatalogHandler.CheckCatalogUpdate(OriginalCatalogPath)).Key > 0)
        {
            image.sprite = defaultImage;
            
            string result = await AddressablesCatalogHandler.DownloadCatalog(OriginalCatalogPath);
            if (!string.IsNullOrEmpty(result))
            {
                PlayerPrefs.SetString("InstalledCatalog", result);
            }
            else
            {
                return;
            }
        }
        
        m_imageHandle = Addressables.LoadAssetAsync<Sprite>("image");
        image.sprite = await m_imageHandle.Task;
        
        m_backgroundHandle = Addressables.LoadAssetAsync<Sprite>("technology-background-1632715");
        background.sprite = await m_backgroundHandle.Task;
    }

    public async void CheckUpdate()
    {
        //If any update is available, display the update screen
        if ((await AddressablesCatalogHandler.CheckCatalogUpdate(NewCatalogPath)).Key > 0)
        {
            m_dlcToDownload = null;
            updateAvailableHolder.SetActive(true);
        }
    }

    public async void DLCCatalogDownload(int index)
    {
        //Index 0 is None in the dropdown
        if (index == 0)
        {
            return;
        }
        
        //If any update is available, display the update screen
        if ((await AddressablesCatalogHandler.CheckCatalogUpdate(dlcCatalogs[index - 1])).Key > 0)
        {
            m_dlcToDownload = new KeyValuePair<string, string>(dlcCatalogs[index - 1], "DLC" + index);
            updateAvailableHolder.SetActive(true);
        }
    }
    
    public async void DownloadUpdate()
    {
        if (m_dlcToDownload.HasValue)
        {
            ReleaseAsyncOperationHandles();

            Resources.UnloadUnusedAssets();

            string catalogPath = m_dlcToDownload.Value.Key;
            string catalogName = m_dlcToDownload.Value.Value;
            
            //We need to tell the system what was our previously installed catalog so that it clears old data
            string mainResult = await AddressablesCatalogHandler.DownloadCatalog(catalogPath, PlayerPrefs.GetString(catalogName));
            //If download succeeded, update our UI and storage
            if (!string.IsNullOrEmpty(mainResult))
            {
                PlayerPrefs.SetString(catalogName, mainResult);

                if (m_dlcCatalogHandles.TryGetValue(catalogPath, out AsyncOperationHandle<ScriptableObjectExample> operationHandle))
                {
                    if (operationHandle.IsValid())
                    {
                        Addressables.Release(operationHandle);
                    }
                }

                LoadDLCData(catalogName);
            }
        }
        else
        {
            image.sprite = defaultImage;

            ReleaseAsyncOperationHandles();

            Resources.UnloadUnusedAssets();

            //We need to tell the system what was our previously installed catalog so that it clears old data
            string result = await AddressablesCatalogHandler.DownloadCatalog(NewCatalogPath, PlayerPrefs.GetString("InstalledCatalog"));
            //If download succeeded, update our UI and storage
            if (!string.IsNullOrEmpty(result))
            {
                PlayerPrefs.SetString("InstalledCatalog", result);

                LoadData();
            }
        }
    }

    async void LoadData()
    {
        m_imageHandle = Addressables.LoadAssetAsync<Sprite>("image");
        image.sprite = await m_imageHandle.Task;
                
        m_backgroundHandle = Addressables.LoadAssetAsync<Sprite>("technology-background-1632715");
        background.sprite = await m_backgroundHandle.Task;
    }
    
    async void LoadDLCData(string dlc)
    {
        AsyncOperationHandle<ScriptableObjectExample> operationHandle = Addressables.LoadAssetAsync<ScriptableObjectExample>("DLCData");
        m_dlcCatalogHandles[dlc] = operationHandle;

        switch (dlc)
        {
            case "DLC1":
                dlc1Data.text = (await operationHandle.Task).Version;
                break;
            case "DLC2":
                dlc2Data.text = (await operationHandle.Task).Version;
                break;
            case "DLC3":
                dlc3Data.text = (await operationHandle.Task).Version;
                break;
        }
    }

    public void CancelUpdate()
    {
        //This just cancels the download process
        AddressablesCatalogHandler.CancelDownloadingUpdate(NewCatalogPath, m_updateLocator);
    }

    public void DeleteCache()
    {
        background.sprite = null;
        image.sprite = defaultImage;

        dlc1Data.text = "N/A";
        dlc2Data.text = "N/A";
        dlc3Data.text = "N/A";
        
        PlayerPrefs.DeleteKey("InstalledCatalog");
        PlayerPrefs.DeleteKey("MainInstalledCatalog");
        PlayerPrefs.DeleteKey("DLC1InstalledCatalog");
        PlayerPrefs.DeleteKey("DLC2InstalledCatalog");
        
        ReleaseAsyncOperationHandles();

        AddressablesCatalogHandler.DeleteAllCachedData();
    }
    
    void ReleaseAsyncOperationHandles()
    {
        if (m_imageHandle.IsValid())
        {
            Addressables.Release(m_imageHandle);
        }
        if (m_backgroundHandle.IsValid())
        {
            Addressables.Release(m_backgroundHandle);
        }

        foreach (KeyValuePair<string, AsyncOperationHandle<ScriptableObjectExample>> keyValuePair in m_dlcCatalogHandles)
        {
            if (keyValuePair.Value.IsValid())
            {
                Addressables.Release(keyValuePair.Value);
            }
        }

        AddressablesCatalogHandler.UnloadSystem();
        AssetBundle.UnloadAllAssetBundles(false);
    }

}
