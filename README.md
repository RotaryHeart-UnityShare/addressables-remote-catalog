# Addressables Remote Catalog #

Full project that includes a complete example for download handling of remote catalogs. The remote catalogs can be built from a different project.

## What this project provides ##

This project shows a fully working example of how to:

1. Download remote catalogs
2. Get catalog size and ask player for downloading 
   1. Handles canceling update and still be able to play with local catalog
   2. Handles updating to the new catalog
3. Delete all the cached data

It also has custom logic that in my opinion should be handled by the Addressables system for:

1. Clearing cache data (Addressables implementation is throwing errors)
2. Resetting addressables completely (No available implementation)
3. Caching downloaded remote catalog in a way that prevents auto downloading updates (No available implementation)

## Multi catalog build project

If you need to be able to build multiple catalogs from the same project, check this fork I've made from [Addressables - Multi-Catalog](https://github.com/juniordiscart/com.unity.addressables) https://github.com/RotaryHeart/com.unity.addressables